import { URL_API } from './base'

export function ConsultarResumo(callback){
    fetch(`${URL_API}/resumo`).then(resultado => resultado.json().then(callback));
}